'use strict';

function enableEvents(Schema) {

  Schema.post('save', _save);
  Schema.post('update', _update);
  Schema.post('findOneAndUpdate', _update);
  Schema.post('remove', _remove);
  Schema.post('findOneAndRemove', _remove);

  function _save(doc) {
    Schema.emit('model-event', 'save', doc);
  }

  function _update(doc) {
    Schema.emit('model-event', 'update', doc);
  }

  function _remove(doc) {
    Schema.emit('model-event', 'remove', doc);
  }
}


module.exports = enableEvents;

