module.exports = [
  {
    "name": "Boyd Olsen",
    "company": "FLUMBO"
  },
  {
    "name": "Tanner Parrish",
    "company": "FIBEROX"
  },
  {
    "name": "Dana Serrano",
    "company": "COREPAN"
  },
  {
    "name": "Brittney Dawson",
    "company": "KRAGGLE"
  },
  {
    "name": "Stephens Carr",
    "company": "COMBOGEN"
  },
  {
    "name": "Michael Stein",
    "company": "EYEWAX"
  },
  {
    "name": "Deirdre Burris",
    "company": "IMKAN"
  },
  {
    "name": "Albert Andrews",
    "company": "FIBEROX"
  }
];