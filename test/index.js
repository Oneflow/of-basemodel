'use strict';


var mongoose    = require('mongoose'),
    express     = require('express'),
    should      = require('should'),
    bluebird    = require('bluebird'),
    _           = require('lodash'),
    ofBaseModel = require('../index'),
    collection  = require('./data/collection');

mongoose.Promise = bluebird;


describe('ofBaseModel', function () {
  var schema;
  var Model;
  before(function (done) {
    mongoose.connect(process.env.MONGODB);
    mongoose.connection.on('error', function (err) {
      console.log(err);
      throw "Could not connect to DB";
    });

    mongoose.connection.on('open', done);
  });

  before(function (done) {
    schema = new mongoose.Schema({
      name: {
        type: String,
        searchable: true
      },
      company: String
    });
    schema.plugin(ofBaseModel);
    Model = mongoose.model('Model', schema);
    done();
  });

  before(function (done) {
    Model.remove({})
     .then(function () {
       Model.create(collection, function (err, docs) {
         done();
       });
     });
  });


  it('should have crud methods', function (done) {
    schema.statics.$read.should.be.Function();
    schema.statics.$search.should.be.Function();
    done();
  });

  it('where filtering should work', function (done) {
    Model.$search({
       where: {
         company: 'FIBEROX'
       }
     })
     .then(function (res) {
       res.items.should.have.length(2);
       res.count.should.be.equal(2);
       res.items[0].company.should.be.equal('FIBEROX');
       done();
     });
  });

  it('pagination should work', function (done) {
    Model.$search({
       pagesize: 2,
       page: 2
     })
     .then(function (res) {
       res.items.should.have.length(2);
       res.count.should.be.equal(8);
       done();
     });
  });

  it('sorting should work', function (done) {

    bluebird.all([
      Model.$search({sort: 'name'}),
      Model.$search({sort: '-name'})
    ]).then(function (results) {
      var down = results[0];
      var up   = results[1];
      down.items[0].name.should.be.equal('Albert Andrews');
      up.items[0].name.should.be.equal('Tanner Parrish');
      done();
    });
  });

  it('selecting should work', function (done) {
    Model.$search({
       select: 'name -_id'
     })
     .then(function (res) {
       res.items[0].should.have.property('name');
       should(res.items[0].company).be.Undefined();
       done();
     });
  });

  it('search q should work', function (done) {
    Model.$search({
       q: 'rittney'
     })
     .then(function (res) {
       res.items[0].name.should.be.equal('Brittney Dawson');
       done();
     });
  });

  it('lean should work', function (done) {
    Model.$search({
       lean: true
     })
     .then(function (res) {
       var oLenght = Object.keys(res.items[0]).length;
       oLenght.should.be.equal(4);
       done();
     });
  });

  it('should emit events', function (done) {
    var numEvents = 0;
    Model.schema.on('model-event', function (eventType, doc) {
      numEvents++;
      if (numEvents === 2) {
        done();
      }
    });

    Model.update({company: 'FLUMBO'}, {company: 'FLUMBOO'}).exec();
    var person = new Model();
    person.save();
  });
});