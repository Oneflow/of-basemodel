'use strict';

var _            = require('lodash');
var utilities    = require('./lib/utilities');
var enableCrud   = require('./lib/crud-methods');
var enableEvents = require('./lib/events');


function OfBaseModel(schema, _options) {
  var _defaultOptions = {rest: true, events: true};
  //Check schema
  if (schema.constructor.name !== 'Schema') {
    throw new Error('Not a valid schema');
  }
  //parse options
  var options = utilities.parseArgs(_options, _defaultOptions);

  if (options.rest === true) {
    enableCrud(schema);
  }
  if (options.events) {
    enableEvents(schema);
  }
  /*if (options.socketIO) {
   enableSocketIOEvents(options.socketIO);
   }*/
}

module.exports = OfBaseModel;
